package com.unitec.interfaces;

/**
 *
 * @author Carlos
 */
public interface FiguraGeometrica {
    
    double getArea();
    double getPerimetro();
    String getNombreFigura();
}
