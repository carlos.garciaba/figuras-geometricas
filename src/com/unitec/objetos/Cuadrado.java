package com.unitec.objetos;

/**
 *
 * @author Carlos GB
 */
public class Cuadrado extends Rectangulo{
    
    public Cuadrado(double lado){
        super(lado,lado);
    }

    @Override
    public void setLado2(double lado2) {
        super.setLado1(lado2);
        super.setLado2(lado2);
    }

    @Override
    public void setLado1(double lado1) {
        this.setLado2(lado1);
    }

    @Override
    public String getNombreFigura() {
        return "Cuadrado";
    }    
    
}
