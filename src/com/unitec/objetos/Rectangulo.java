/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unitec.objetos;

import com.unitec.interfaces.FiguraGeometrica;

/**
 *
 * @author Carlos GB
 */
public class Rectangulo implements FiguraGeometrica{

    private double lado1;
    private double lado2;
    
    public Rectangulo(double lado1,double lado2){
        this.lado1=lado1;
        this.lado2=lado2;
    }
    
    @Override
    public double getArea() {
        return lado1*lado2;
    }

    @Override
    public double getPerimetro() {
        return 2*lado1+2*lado2;
    }

    public double getLado1() {
        return lado1;
    }

    public void setLado1(double lado1) {
        this.lado1 = lado1;
    }

    public double getLado2() {
        return lado2;
    }

    public void setLado2(double lado2) {
        this.lado2 = lado2;
    }

    @Override
    public String getNombreFigura() {
        return "Rectamgulo";
    }
    
}
