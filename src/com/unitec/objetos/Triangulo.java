package com.unitec.objetos;

/**
 *
 * @author Carlos GB
 */
public class Triangulo extends Rectangulo{
    
    public Triangulo(double lado1,double lado2){
        super(lado1,lado2);
    }

    @Override
    public double getPerimetro() {
        return getLado1()+getLado2()+getLado3();
    }

    @Override
    public double getArea() {
        return super.getArea()/2;
    }

    @Override
    public String getNombreFigura() {
        return "Triangulo";
    }   
    
    public double getLado3() {
        return Math.sqrt(Math.pow(getLado1(), 2)+Math.pow(getLado2(), 2));
    }
    
}
