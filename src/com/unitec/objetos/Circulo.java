package com.unitec.objetos;

import com.unitec.interfaces.FiguraGeometrica;

/**
 *
 * @author Carlos GB
 */
public class Circulo implements FiguraGeometrica{

    private double radio;
    
    public Circulo(double radio){ this.radio=radio;}
    
    @Override
    public double getArea() {
        return 2*Math.PI*radio;
    }

    @Override
    public double getPerimetro() {
        return Math.pow(radio, 2)*Math.PI;
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    @Override
    public String getNombreFigura() {
        return "Circulo";
    }
    
    
    
}
