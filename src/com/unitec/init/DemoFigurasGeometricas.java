package com.unitec.init;

import com.unitec.interfaces.FiguraGeometrica;
import com.unitec.objetos.Circulo;
import com.unitec.objetos.Cuadrado;
import com.unitec.objetos.Rectangulo;
import com.unitec.objetos.Triangulo;
import java.util.Scanner;

/**
 *
 * @author Carlos GB
 */
public class DemoFigurasGeometricas {

    public static void main(String[] args) {
        int opcion = 0;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("====Calculadora Geometrica====");
            System.out.println("Selecciona una opcion:");
            System.out.println("1. Triangulo");
            System.out.println("2. Cudrado");
            System.out.println("3. Rectangulo");
            System.out.println("4. Circulo");
            System.out.println("5. Salir");
            try {
                opcion = Integer.parseInt(scan.nextLine());
            } catch (NumberFormatException e) {
                opcion=0;
                System.out.println("Opcion invalida");
            }
            double lado1=0,lado2=0;
            FiguraGeometrica figura=null;
            switch (opcion) {
                case 1:
                    System.out.println("Lado 1:");
                    lado1=Double.parseDouble(scan.nextLine());
                    System.out.println("Lado 2:");
                    lado2=Double.parseDouble(scan.nextLine());
                    figura=new Triangulo(lado1, lado2);
                    break;
                case 2:
                    System.out.println("Lado:");
                    lado1=Double.parseDouble(scan.nextLine());
                    figura=new Cuadrado(lado1);
                    break;
                case 3:
                    System.out.println("Lado 1:");
                    lado1=Double.parseDouble(scan.nextLine());
                    System.out.println("Lado 2:");
                    lado2=Double.parseDouble(scan.nextLine());
                    figura=new Rectangulo(lado1, lado2);
                    break;
                case 4:
                    System.out.println("Radio:");
                    lado1=Double.parseDouble(scan.nextLine());
                    figura=new Circulo(lado1);
                    break;
            }
            if(figura!=null)
            imprimeInfofigura(figura);
        } while (opcion != 5);
        System.exit(0);
    }

    private static void imprimeInfofigura(FiguraGeometrica figura) {
        System.out.println("******************");
        System.out.println(String.format("Nombre de la figura: %s", figura.getNombreFigura()));
        System.out.println(String.format("Areaa: %.2f", figura.getArea()));
        System.out.println(String.format("Perimetro: %.2f", figura.getPerimetro()));
        System.out.println("******************");
    }

}
